#pragma checksum "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "916612dd4088f99cf2a947b1e68cc8bac2e7c738"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace DoppelzahnChatService.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using DoppelzahnChatService;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using DoppelzahnChatService.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using BlazorChatSample.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\_Imports.razor"
using BlazorChatSample.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\Pages\Index.razor"
using BlazorChatSample.Shared;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 56 "C:\Users\3849824527\Desktop\DoppelzahnChatService\DoppelzahnChatService\Pages\Index.razor"
        
    // flag to indicate chat status
    bool chatting = false;

    // name of the user who will be chatting
    string username = null;

    ChatClient client = null;

    // on-screen message
    string message = null;

    // new message input
    string newMessage = null;

    // list of messages in chat
    List<Message> messages = new List<Message>();

    /// <summary>
    /// Start chat client
    /// </summary>
    async Task Chat()
    {
        // check username is valid
        if (string.IsNullOrWhiteSpace(username))
        {
            message = "Please enter a name";
            return;
        };

        try
        {
            // remove old messages if any
            messages.Clear();

            // Create the chat client
            string baseUrl = navigationManager.BaseUri;
            client = new ChatClient(username, baseUrl);
            // add an event handler for incoming messages
            client.MessageReceived += MessageReceived;
            // start the client
            Console.WriteLine("Index: chart starting...");
            await client.StartAsync();
            Console.WriteLine("Index: chart started?");

            chatting = true;
        }
        catch (Exception e)
        {
            message = $"ERROR: Failed to start chat client: {e.Message}";
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
        }
    }

    /// <summary>
    /// Inbound message
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void MessageReceived(object sender, MessageReceivedEventArgs e)
    {
        Console.WriteLine($"Blazor: receive {e.Username}: {e.Message}");
        bool isMine = false;
        if (!string.IsNullOrWhiteSpace(e.Username))
        {
            isMine = string.Equals(e.Username, username, StringComparison.CurrentCultureIgnoreCase);
        }

        var newMsg = new Message(e.Username, e.Message, isMine);
        messages.Add(newMsg);

        // Inform blazor the UI needs updating
        StateHasChanged();
    }

    async Task DisconnectAsync()
    {
        if (chatting)
        {
            await client.StopAsync();
            client = null;
            message = "chat ended";
            chatting = false;
        }
    }

    async Task SendAsync()
    {
        if (chatting && !string.IsNullOrWhiteSpace(newMessage))
        {
            // send message to hub
            await client.SendAsync(newMessage);
            // clear input box
            newMessage = "";
        }
    }

    class Message
    {
        public Message(string username, string body, bool mine)
        {
            Username = username;
            Body = body;
            Mine = mine;
        }

        public string Username { get; set; }
        public string Body { get; set; }
        public bool Mine { get; set; }

        /// <summary>
        /// Determine CSS classes to use for message div
        /// </summary>
        public string CSS
        {
            get
            {
                return Mine ? "sent" : "received";
            }
        }
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
    }
}
#pragma warning restore 1591
